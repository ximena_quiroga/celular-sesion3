

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CelularTest.
 *
 * @author
 */
public class CelularTest
{
    @Test
    public void testCambiarBateria()
    {
        Bateria bateria = new Bateria();
        Celular celular = new Celular(bateria);
        celular.cambiarEstadoDeBateria("bueno");
        
        boolean estadoBateria = celular.mostrarCambioDeBateriaRequerido();
        
        assertFalse(estadoBateria);
    }
    
    @Test
    public void testCambiarBateriaMalEstado()
    {
        Bateria bateria = new Bateria();
        Celular celular = new Celular(bateria);
        celular.cambiarEstadoDeBateria("malo");
        
        boolean estadoBateria = celular.mostrarCambioDeBateriaRequerido();
        
        assertTrue(estadoBateria);
    }
    
    @Test
    public void testEncenderRadio()
    {
        Radio radio = new Radio();
        Celular celular = new Celular(radio);
        celular.encenderRadio();
        
        boolean estadoRadio = celular.mostrarEstadoRadioEncendida();
        
        assertTrue(estadoRadio);
    }
}
