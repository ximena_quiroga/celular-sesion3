
/**
 * Write a description of class Celular here.
 * 
 * @author
 */
public class Celular
{
    private Bateria bateria;
    private Radio radio;
    private boolean cambioDeBateriaRequerido;

    /**
     * Constructor for objects of class Celular
     */
    public Celular(Bateria bateria)
    {
        this.bateria = bateria;
    }
    
    /**
     * Constructor for objects of class Celular
     */
    public Celular(Radio radio)
    {
        this.radio = radio;
    }
    
    /**
     * Constructor for objects of class Celular
     */
    public Celular(Bateria bateria, Radio radio)
    {
        this.bateria = bateria;
        this.radio = radio;
    }
    
    /**
     * Cambiar estado de la bateria.
     */
    public void cambiarEstadoDeBateria(String estado)
    {
        bateria.cambiarEstado(estado);
        esCambioDeBateriaRequerido();
    }
    
    /**
     * muestra el estado de la bateria del celular.
     * 
     * @return un valor String que indica el estado de la bateria (optimo, bueno, malo)
     */
    private String mostrarEstadoDeLaBateria()
    {
        return bateria.mostrarEstado();
    }
    
    /**
     * verifica si el estado de la bateria es malo y cambia el estado de cambioDeBateriaRequerido.
     *
     */
    private void esCambioDeBateriaRequerido()
    {
        if("malo".equals(mostrarEstadoDeLaBateria())) {
            cambioDeBateriaRequerido = true;
        }
        else {
            cambioDeBateriaRequerido = false;
        }
    }
    
    /**
     * muestra el valor de cambioDeBateriaRequerido.
     * 
     * @return un valor String que indica el estado de la bateria (optimo, bueno, malo)
     */
    public boolean mostrarCambioDeBateriaRequerido()
    {
        return cambioDeBateriaRequerido;
    }


    /**
     * Enciende la radio del celular.
     */
    public void encenderRadio()
    {
        radio.encender();
    }
    
    /**
     * muestra el estado de encendido de la radio del celular.
     * 
     * @return un valor boolean que indica si la radio esta prendida (true), o apagada (false)
     */
    public boolean mostrarEstadoRadioEncendida()
    {
        return radio.mostrarEstadoDeEncendido();
    }
}
