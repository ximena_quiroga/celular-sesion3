
/**
 * Write a description of class Bateria here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bateria
{
    // Estados: optimo, bueno, malo
    private String estado;

    /**
     * Constructor for objects of class Bateria
     */
    public Bateria()
    {
        estado = "optimo";
    }

    /**
     * Modifica el estado de la bateria.
     * 
     * @param  estado   el nuevo estado de la bateria.
     */
    public void cambiarEstado(String estado)
    {
        this.estado = estado;
    }
    
    /**
     * Modifica el estado de la bateria.
     * 
     * @return  estado actual de la bateria.
     */
    public String mostrarEstado()
    {
        return estado;
    }
}
