
/**
 * Representa un Radio.
 * 
 * @author
 */
public class Radio
{
    private String marca;
    private boolean estadoDeEncendido;

    /**
     * Constructor for objects of class Radio
     */
    public Radio()
    {
        estadoDeEncendido = false;
    }

    /**
     * Enciende la radio;
     * 
     */
    public void encender()
    {
        estadoDeEncendido = true;
    }
    
    /**
     * Muestra el estado de encendido de la radio
     * 
     * @return     un tipo boolean que indica si la radio esta encendida (true) o apagada (false).
     */
    public boolean mostrarEstadoDeEncendido()
    {
        return estadoDeEncendido;
    }
}
