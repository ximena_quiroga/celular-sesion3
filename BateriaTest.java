

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class BateriaTest.
 *
 * @author
 */
public class BateriaTest
{
    @Test
    public void testEstadoBateria()
    {
        Bateria bateria = new Bateria();
        bateria.cambiarEstado("malo");
        String estado = bateria.mostrarEstado();
        
        assertEquals("malo", estado);
    }
}
