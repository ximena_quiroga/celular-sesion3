

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class RadioTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class RadioTest
{
    @Test
    public void testEncendido()
    {
        Radio radio = new Radio();
        radio.encender();
        boolean encendida = radio.mostrarEstadoDeEncendido();
        
        assertTrue(encendida);
    }
}
