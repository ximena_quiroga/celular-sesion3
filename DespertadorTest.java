

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class DespertadorTest.
 *
 * @author
 */
public class DespertadorTest
{
    @Test
    public void testEncenderRadio()
    {
        Radio radio = new Radio();
        Despertador despertador = new Despertador(radio);        
        despertador.encenderRadio();
        boolean estadoRadio = despertador.mostrarEstadoRadioEncendida();
        
        assertTrue(estadoRadio);
    }
    
    @Test
    public void testEncenderRadioValor()
    {
        Despertador despertador = new Despertador();
        Radio radio = new Radio();
        despertador.encenderRadio(radio);
        boolean estadoRadio = radio.mostrarEstadoDeEncendido();
        
        assertTrue(estadoRadio);
    }
}
