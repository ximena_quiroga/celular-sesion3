
/**
 * Write a description of class Despertador here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Despertador
{
    // instance variables - replace the example below with your own
    private Radio radio;
    
    /**
     * Constructor for objects of class Despertador
     */
    public Despertador()
    {
    }

    /**
     * Constructor for objects of class Despertador
     */
    public Despertador(Radio radio)
    {
        this.radio = radio;
    }

    /**
     * Enciende la radio
     */
    public void encenderRadio()
    {
        radio.encender();
    }
    
    /**
     * Enciende la radio
     */
    public void encenderRadio(Radio radio)
    {
        radio.encender();
    }
    
    /**
     * muestra el estado de encendido de la radio del celular.
     * 
     * @return un valor boolean que indica si la radio esta prendida (true), o apagada (false)
     */
    public boolean mostrarEstadoRadioEncendida()
    {
        return radio.mostrarEstadoDeEncendido();
    }
}
